**Extension for hybris that allows for runtime changes to storefront and email messages by saving the translations into the database.**

1. add internationalization to your localextensions.xml
    `<extension name="internationalization"/>`
2. add the dependency to your core extension
    `<requires-extension name="internationalization"/>`
3. alias the message source to point to the database message source
    `<alias name="databaseTranslationsMessageSource" alias="messageSource" />`
    If you want to also fallback to the messages found in properties files, then set the parent message of the 
        databaseTranslationsMessageSource to be baseMessageSource
    `<alias name="myDatabaseTranslationsMessageSource" alias="messageSource" />
    <bean id="myDatabaseTranslationsMessageSource" parent="databaseTranslationsMessageSource"
          class="com.istone.internationalization.context.support.DatabaseTranslationsMessageSource">
        <property name="parentMessageSource" ref="baseMessageSource"/>
    </bean>`
    If you set emptyIfNotFound to false, the fallback to baseMessageSource will not work anymore.
4. configure themeSource in your storefront to use as parent message source the database message source 
        and remove the alias for messageSource
       `<alias name="storefrontMessageSource" alias="themeSource" />
        <bean id="storefrontMessageSource" class="mypackage.storefront.web.theme.StorefrontResourceBundleSource">
            ...
            <property name="parentMessageSource" ref="myDatabaseTranslationsMessageSource" />
        </bean>`
6. in order to translate email properties
    a. alias emailTemplateTranslationStrategy to point to messageSourceEmailTemplateTranslationStrategy in your core extension
        `<alias name="messageSourceEmailTemplateTranslationStrategy" alias="emailTemplateTranslationStrategy"/>`
    b. add messagePrefix at the start of the template script attached to the RendererTemplate of your email
         `## messagePrefix=email-forgottenPassword`
    c. update the RendererTemplate's template script in the database
    d. insert all the translations for that email using the prefix 
        All messages must be inserted otherwise there is exception when generating email. 
        No fallback to messages coming from properties occurs.
        `INSERT_UPDATE LocalizationEntry;code[unique=true];translation[lang=en]
        ;email-forgottenPassword.emailSubject;Forgotten Password;`
