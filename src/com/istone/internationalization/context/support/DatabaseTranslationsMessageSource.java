package com.istone.internationalization.context.support;

import com.istone.internationalization.data.LocalizationEntryData;
import com.istone.internationalization.service.LocalizationEntryService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.HierarchicalMessageSource;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.MessageSourceSupport;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.Locale;


public class DatabaseTranslationsMessageSource extends MessageSourceSupport implements HierarchicalMessageSource {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseTranslationsMessageSource.class);

    private MessageSource parentMessageSource;
    private LocalizationEntryService localizationEntryService;
    private String missingTranslationStartTag = "<span style=\"color:red;\">";
    private String missingTranslationEndTag = "</span>";
    private Boolean emptyIfNotFound = true;

    protected MessageFormat resolveCode(String code, Locale locale) {
        String message = null;
        try {
            LocalizationEntryData entry = getLocalizationEntryService().getLocalizationEntry(code);
            try {
                message = entry.getTranslations().get(locale);
            } catch (Exception e) {
                LOG.debug("Exception caught while getting translation for locale {}.", locale.toString(), e);
            }

            if (StringUtils.isEmpty(message)) {
                LOG.debug("No database translation for code {} found - translate!", code);
                if (getEmptyIfNotFound()) {
                    return null;
                }
                message = getMissingTranslationStartTag() + code + getMissingTranslationEndTag();
            }
            return new MessageFormat(message, locale);
        } catch (UnknownIdentifierException e) {
            LOG.debug("No database translation for code {} found - translate!", code);
            if (getEmptyIfNotFound()) {
                return null;
            }
        } catch (Exception e) {
            LOG.warn("Unknown error while loading frontend-text for {} in locale {} ", code, locale, e);
        }
        code = escapeMessageCode(code);
        message = getMissingTranslationStartTag() + code + getMissingTranslationEndTag();
        return new MessageFormat(message, locale);
    }

    protected String escapeMessageCode(String code) {
        return code.replaceAll("([{}])", "'$1'");
    }

    @Override
    public final String getMessage(String code, Object[] arguments, String defaultMessage, Locale locale) {
        String message = getMessageInternal(code, arguments, locale);
        return message != null ? message : renderDefaultMessage(defaultMessage, arguments, locale);
    }

    @Override
    public final String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException {
        String message = getMessageInternal(code, args, locale);
        if (message != null) {
            return message;
        } else {
            throw new NoSuchMessageException(code, locale);
        }
    }

    @Override
    public final String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
        String[] codes = resolvable.getCodes();
        if (codes == null) {
            codes = new String[0];
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <= codes.length; i++) {
            String code = codes[i];
            String message = getMessageInternal(code, resolvable.getArguments(), locale);
            if (message != null) {
                if (!message.contains(code)) {
                    return message;
                }

                if (stringBuilder.length() > 0) {
                    stringBuilder.append(", ");
                }

                stringBuilder.append(message);
            }
        }

        return stringBuilder.toString();
    }

    protected String getMessageInternal(String code, Object[] arguments, Locale locale) {
        if (code == null) {
            return null;
        } else {
            if (locale == null) {
                locale = Locale.getDefault();
            }

            Object[] argumentsToUse = arguments;
            if (!isAlwaysUseMessageFormat() && ObjectUtils.isEmpty(arguments)) {
                String message = resolveCodeWithoutArguments(code, locale);
                if (message != null) {
                    return message;
                }
            } else {
                argumentsToUse = resolveArguments(arguments, locale);
                MessageFormat messageFormat = resolveCode(code, locale);
                if (messageFormat != null) {
                    return messageFormat.format(argumentsToUse);
                }
            }

            return getMessageFromParent(code, argumentsToUse, locale);
        }
    }

    protected String resolveCodeWithoutArguments(String code, Locale locale) {
        MessageFormat messageFormat = resolveCode(code, locale);
        if (messageFormat != null) {
            return messageFormat.format(new Object[0]);
        } else {
            return null;
        }
    }

    protected String getMessageFromParent(String code, Object[] arguments, Locale locale) {
        MessageSource parent = getParentMessageSource();
        if (parent != null) {
            return parent instanceof DatabaseTranslationsMessageSource ?
                    ((DatabaseTranslationsMessageSource) parent).getMessageInternal(code, arguments, locale) :
                    parent.getMessage(code, arguments, null, locale);
        } else {
            return null;
        }
    }

    protected String getMissingTranslationStartTag() {
        return this.missingTranslationStartTag;
    }

    public void setMissingTranslationStartTag(String missingTranslationStartTag) {
        this.missingTranslationStartTag = missingTranslationStartTag;
    }

    protected String getMissingTranslationEndTag() {
        return this.missingTranslationEndTag;
    }

    public void setMissingTranslationEndTag(String missingTranslationEndTag) {
        this.missingTranslationEndTag = missingTranslationEndTag;
    }

    protected Boolean getEmptyIfNotFound() {
        return this.emptyIfNotFound;
    }

    public void setEmptyIfNotFound(Boolean emptyIfNotFound) {
        this.emptyIfNotFound = emptyIfNotFound;
    }

    protected LocalizationEntryService getLocalizationEntryService() {
        return localizationEntryService;
    }

    @Required
    public void setLocalizationEntryService(LocalizationEntryService localizationEntryService) {
        this.localizationEntryService = localizationEntryService;
    }

    public MessageSource getParentMessageSource() {
        return this.parentMessageSource;
    }

    public void setParentMessageSource(MessageSource parent) {
        this.parentMessageSource = parent;
    }
}
