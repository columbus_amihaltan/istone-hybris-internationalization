package com.istone.internationalization.mail.strategies.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;

import java.util.HashMap;
import java.util.Locale;

public class MessageSourceMap extends HashMap<String, Object> {
    private final static Object[] EMPTY_ARGS = new Object[0];
    private Locale locale;
    private MessageSource messageSource;
    private String messagePrefix;

    public MessageSourceMap(Locale locale, MessageSource messageSource, String messagePrefix) {
        this.locale = locale;
        this.messageSource = messageSource;
        this.messagePrefix = StringUtils.isNotEmpty(messagePrefix) ? messagePrefix + "." : StringUtils.EMPTY;
    }

    @Override
    public Object get(Object key) {
        return key instanceof String ? getMessage((String) key) : super.get(key);
    }

    public String getMessage(String code) {
        return getMessage(code, EMPTY_ARGS, locale);
    }

    public String getMessage(String code, Object[] parameters) {
        return getMessage(code, parameters, locale);
    }

    public String getMessage(String code, Object[] parameters, Locale locale) {
        return messageSource.getMessage(prefixMessageCode(code), parameters, locale);
    }

    protected String prefixMessageCode(String messageCode) {
        return messagePrefix + messageCode;
    }
}
