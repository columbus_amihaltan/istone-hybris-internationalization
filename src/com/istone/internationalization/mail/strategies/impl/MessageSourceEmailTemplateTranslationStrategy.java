package com.istone.internationalization.mail.strategies.impl;

import de.hybris.platform.acceleratorservices.process.strategies.impl.DefaultEmailTemplateTranslationStrategy;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.model.media.MediaModel;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Map;

public class MessageSourceEmailTemplateTranslationStrategy extends DefaultEmailTemplateTranslationStrategy {
    private static final Logger LOG = LoggerFactory.getLogger(MessageSourceEmailTemplateTranslationStrategy.class);
    private MessageSource messageSource;

    public MessageSourceEmailTemplateTranslationStrategy() {
    }

    @Override
    public Map<String, Object> translateMessagesForTemplate(RendererTemplateModel renderTemplate, String languageIso) {
        Locale locale = getLocale(languageIso);
        return new MessageSourceMap(locale, getMessageSource(), getMessagePrefix(renderTemplate, locale));
    }

    protected String getMessagePrefix(RendererTemplateModel renderTemplate, Locale locale) {
        MediaModel mediaModel = renderTemplate.getContent(locale);
        return mediaModel != null ? getMessagePrefix(mediaModel) : StringUtils.EMPTY;
    }

    protected String getMessagePrefix(MediaModel mediaModel) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(getMediaService().getStreamFromMedia(mediaModel), StandardCharsets.UTF_8));

            String line;
            //read first lines and see if there is a messagePrefix to be used for the database translation
            for (int count = 0; (line = reader.readLine()) != null && count <= 5; ++count) {
                String messagePrefix = getMessagePrefix(line);
                if (StringUtils.isNotEmpty(messagePrefix)) {
                    return messagePrefix;
                }
            }
        } catch (IOException e) {
            LOG.warn("could not get message prefix from vm file for media {}", mediaModel.getCode(), e);
        } finally {
            IOUtils.closeQuietly(reader);
        }

        return StringUtils.EMPTY;
    }

    protected String getMessagePrefix(String line) {
        String trimmedLine = StringUtils.trim(line);
        if (trimmedLine.contains("## messagePrefix")) {
            return StringUtils.substringAfter(trimmedLine, "=");
        }
        return null;
    }

    protected Locale getLocale(String languageIso) {
        return LocaleUtils.toLocale(StringUtils.defaultString(languageIso, "en"));
    }

    protected MessageSource getMessageSource() {
        return messageSource;
    }

    @Required
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
}

