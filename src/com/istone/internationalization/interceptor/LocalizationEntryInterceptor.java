package com.istone.internationalization.interceptor;

import com.istone.internationalization.event.LocalizationEntryChangedEvent;
import com.istone.internationalization.model.LocalizationEntryModel;
import com.istone.internationalization.service.LocalizationEntryService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import org.springframework.beans.factory.annotation.Required;

public class LocalizationEntryInterceptor implements PrepareInterceptor<LocalizationEntryModel>, RemoveInterceptor<LocalizationEntryModel> {
    private LocalizationEntryService localizationEntryService;
    private EventService eventService;

    @Override
    public void onPrepare(LocalizationEntryModel localizationEntry, InterceptorContext interceptorContext) {
        evictTranslationCache(localizationEntry.getCode());
    }

    @Override
    public void onRemove(LocalizationEntryModel localizationEntry, InterceptorContext interceptorContext) {
        evictTranslationCache(localizationEntry.getCode());
    }

    protected void evictTranslationCache(String code) {
        getLocalizationEntryService().evictCachedLocalizationEntry(code);
        getEventService().publishEvent(new LocalizationEntryChangedEvent(code));
    }

    protected LocalizationEntryService getLocalizationEntryService() {
        return localizationEntryService;
    }

    @Required
    public void setLocalizationEntryService(LocalizationEntryService localizationEntryService) {
        this.localizationEntryService = localizationEntryService;
    }

    protected EventService getEventService() {
        return eventService;
    }

    @Required

    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }


}
