package com.istone.internationalization.service.impl;

import com.istone.internationalization.data.LocalizationEntryData;
import com.istone.internationalization.model.LocalizationEntryModel;
import com.istone.internationalization.service.LocalizationEntryService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;

public class DefaultLocalizationEntryService implements LocalizationEntryService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultLocalizationEntryService.class);

    private boolean cacheFailureLogged = false;
    private List<Locale> usedLocales;

    private GenericDao<LocalizationEntryModel> localizationEntryDao;
    private CommonI18NService commonI18NService;
    private CacheManager cacheManager;

    @Override
    public LocalizationEntryData getLocalizationEntry(String code) {
        Cache cache = null;
        if (getCacheManager() != null) {
            cache = getCacheManager().getCache("TRANSLATIONS");
        } else {
            LOG.info("No cacheManger injected! Messages are not cached!!");
        }

        if (cache != null) {
            Cache.ValueWrapper valueWrapper = cache.get(code);
            if (valueWrapper != null) {
                return (LocalizationEntryData) valueWrapper.get();
            }
        } else {
            if (!cacheFailureLogged) {
                LOG.warn("No translation cache available! Add cache 'TRANSLATIONS' to your ehcache configuration");
            }
            cacheFailureLogged = true;
        }

        LOG.debug("Loading translation for code {} from database.", code);
        List<LocalizationEntryModel> localizationEntries = getLocalizationEntryDao().find(Collections.singletonMap("code", code));
        validateIfSingleResult(localizationEntries, LocalizationEntryModel.class, "code", code);
        LocalizationEntryData localizationEntry = convertToData(code, localizationEntries.get(0));
        if (cache != null) {
            cache.put(code, localizationEntry);
        }

        return localizationEntry;
    }

    @Override
    public void evictCachedLocalizationEntry(String code) {
        Cache cache = null;
        if (getCacheManager() != null) {
            cache = getCacheManager().getCache("TRANSLATIONS");
        } else {
            LOG.info("No cacheManger injected! Messages are not cached!!");
        }
        if (cache != null) {
            cache.evict(code);
            LOG.debug("evicted translations cache for code {}", code);

        }

    }

    protected LocalizationEntryData convertToData(String code, LocalizationEntryModel localizationEntryModel) {
        LocalizationEntryData localizationEntry = new LocalizationEntryData();
        localizationEntry.setCode(code);
        localizationEntry.setTranslations(new HashMap<>(getUsedLocales().size()));

        getUsedLocales().forEach(locale -> localizationEntry.getTranslations().put(
                locale, localizationEntryModel.getTranslation(locale)));
        return localizationEntry;
    }

    protected List<Locale> getUsedLocales() {
        if (usedLocales == null) {
            usedLocales = new ArrayList<>();
            List<LanguageModel> allLanguages = getCommonI18NService().getAllLanguages();
            allLanguages.forEach(language -> usedLocales.add(getCommonI18NService().getLocaleForLanguage(language)));
        }

        return usedLocales;
    }

    protected GenericDao<LocalizationEntryModel> getLocalizationEntryDao() {
        return localizationEntryDao;
    }

    @Required
    public void setLocalizationEntryDao(GenericDao<LocalizationEntryModel> localizationEntryDao) {
        this.localizationEntryDao = localizationEntryDao;
    }

    protected CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    protected CacheManager getCacheManager() {
        return cacheManager;
    }

    @Required
    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }
}
