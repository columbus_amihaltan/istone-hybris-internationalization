package com.istone.internationalization.event;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

public class LocalizationEntryChangedEvent extends AbstractEvent implements ClusterAwareEvent {
    private String code;

    public LocalizationEntryChangedEvent(String code) {
        this.code = code;
    }

    @Override
    public boolean publish(int sourceNodeId, int targetNodeId) {
        return sourceNodeId != targetNodeId;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String translationCode) {
        this.code = translationCode;
    }
}
