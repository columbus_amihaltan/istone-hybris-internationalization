package com.istone.internationalization.event;

import com.istone.internationalization.service.LocalizationEntryService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import org.springframework.beans.factory.annotation.Required;

public class LocalizationEntryChangedEventListener extends AbstractEventListener<LocalizationEntryChangedEvent> {

    private LocalizationEntryService localizationEntryService;

    @Override
    protected void onEvent(LocalizationEntryChangedEvent event) {
        getLocalizationEntryService().evictCachedLocalizationEntry(event.getCode());
    }

    protected LocalizationEntryService getLocalizationEntryService() {
        return localizationEntryService;
    }

    @Required
    public void setLocalizationEntryService(LocalizationEntryService localizationEntryService) {
        this.localizationEntryService = localizationEntryService;
    }
}
